﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task24.Models
{
    public class Supervisor
    {
        private int id;
        private string name;
        private bool isAvailable;
        private string level;



        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public bool IsAvailable { get => isAvailable; set => isAvailable = value; }
        public string Level { get => level; set => level = value; }
    }
}
