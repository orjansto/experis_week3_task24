﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Task24.Models;

namespace Task24.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            ViewBag.WelcomeMessage = "Testing!";
            ViewBag.Time = DateTime.Now.ToString();
            return View("MyFirstVIew");
        }
        public IActionResult SupervisorInfo()
        {
            Supervisor supervisor = new Supervisor
            {
                Name = "Dean",
                Id = 1,
                IsAvailable = true,
                Level = "Novice"
            };
            return View(supervisor);
        }
        public IActionResult SupervisorInfoList()
        {
            List<Supervisor> supervisors = new List<Supervisor>();
            supervisors.Add(new Supervisor
            {
                Name = "Dean",
                Id = 1,
                IsAvailable = true,
                Level = "pro"
            });
            supervisors.Add(new Supervisor
            {
                Name = "Greg",
                Id = 2,
                IsAvailable = false,
                Level = "over 9000"
            });
            supervisors.Add(new Supervisor
            {
                Name = "Erlend",
                Id = 3,
                IsAvailable = false,
                Level = "unknown"
            });

            return View(supervisors);
        }
    }
}
