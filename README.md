﻿**Experis Academy, Norway**

**Authors:**
* **Ørjan Storås**

# Experis Week3 Task 24
## Task 24


- [x] Create a new ASP.Net Core MVC solution/application
- [x] It must have a new **home page**
- [x] Use the **viewbag** to display the **current time** in the home page
- [x] It must have a **page** to display information about **supervisors** (first just one)
- [x] Upgrade the page to display a **list of supervisors**
- [x] **Do not** use any previously created views/layouts or action methods that comes with the solution
